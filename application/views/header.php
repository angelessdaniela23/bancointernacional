<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">

<!-- bootstrap-->

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<!-- api de google-->
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTqeKCayVcz2foCkSUk4672vXvjT2ohEQ&libraries=places&callback=initMap">
		</script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

		<title>MVC1</title>
		<!-- importacion de swwtaler -->
		<script src=" https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.all.min.js "></script>
		<link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css " rel="stylesheet">
	</head>
	<body>
<!-- importacion de awesome -->


		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <div class="container-fluid">
		    <a class="navbar-brand" href="<?php echo site_url();?>">sigMVC1</a>
		    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		      <span class="navbar-toggler-icon"></span>
		    </button>
		    <div class="collapse navbar-collapse" id="navbarSupportedContent">
		      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
		        <li class="nav-item">
		          <a class="nav-link active" aria-current="page" href="<?php echo site_url();?>">Inicio</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="<?php echo site_url('agencias/index');?>">Hospitales</a>
		        </li>

						<li class="nav-item">
		          <a class="nav-link" href="<?php echo site_url('clinicas/index');?>">Clinica</a>
		        </li>


						<li class="nav-item">
		          <a class="nav-link" href="<?php echo site_url('agencias/index');?>">Agencias</a>
		        </li>


		        <li class="nav-item">
		          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
		        </li>
		      </ul>
		      <form class="d-flex">
		        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
		        <button class="btn btn-outline-success" type="submit">Search</button>
		      </form>
		    </div>
		  </div>
		</nav>
		<div class="container">


		<?php if ($this->session->flashdata('confirmacion')): ?>
			<script type="text/javascript">
			Swal.fire({
				title: "CONFIRMACION",
				text: "<?php echo $this->session->flashdata('confirmacion');?>",
				icon: "success"
			})

			</script>
			<?php $this->session->set_flashdata('confirmacion',''); ?>
		<?php endif; ?>
