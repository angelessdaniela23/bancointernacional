<?php
  class Corresponsal extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("corresponsal",$datos);
      return $respuesta;
    }
    //consulta de datos
    function consultarTodos(){
      $corresponsales=$this->db->get("corresponsal");
      if ($corresponsales->num_rows()>0){
        return $corresponsales->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_co",$id);
      return $this->db->delete("corresponsal");
    }

    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_co",$id);
      $corresponsal=$this->db->get("corresponsal");
      if ($corresponsal->num_rows()>0) {
        return $corresponsal->row();
      } else {
        return false;
      }
    }

    //FUNCION PARA ACTUALIZAR HOSPITALES
    function actualizar($id,$datos){
      $this->db->where("id_co",$id);
      return $this->db->update("corresponsal",$datos);
    }



  } //fin de la clase

 ?>
