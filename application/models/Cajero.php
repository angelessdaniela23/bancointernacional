<?php
  class Cajero extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("cajero",$datos);
      return $respuesta;
    }
    //consulta de datos
    function consultarTodos(){
      $cajeros=$this->db->get("cajero");
      if ($cajeros->num_rows()>0){
        return $cajeros->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_ca",$id);
      return $this->db->delete("cajero");
    }

    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_ca",$id);
      $cajero=$this->db->get("cajero");
      if ($cajero->num_rows()>0) {
        return $cajero->row();
      } else {
        return false;
      }
    }

    //FUNCION PARA ACTUALIZAR HOSPITALES
    function actualizar($id,$datos){
      $this->db->where("id_ca",$id);
      return $this->db->update("cajero",$datos);
    }



  } //fin de la clase

 ?>
