<?php
  class Agencia extends CI_MODEL
  {

    function __construct()
    {
      parent::__construct();
    }
    //insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("agencia",$datos);
      return $respuesta;
    }
    //consulta de datos
    function consultarTodos(){
      $agencias=$this->db->get("agencia");
      if ($agencias->num_rows()>0){
        return $agencias->result();
      } else {
        return false;
      }
    }

    //eliminacion de hopsital por id
    function eliminar($id){
      $this->db->where("id_ag",$id);
      return $this->db->delete("agencia");
    }

    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_ag",$id);
      $agencia=$this->db->get("agencia");
      if ($agencia->num_rows()>0) {
        return $agencia->row();
      } else {
        return false;
      }
    }

    //FUNCION PARA ACTUALIZAR HOSPITALES
    function actualizar($id,$datos){
      $this->db->where("id_ag",$id);
      return $this->db->update("agencia",$datos);
    }



  } //fin de la clase

 ?>
