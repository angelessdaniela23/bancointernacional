<?php

class Agencias extends CI_Controller
{
  function __construct()
  {
      parent::__construct();
      $this->load->model("Agencia");

      // deshabilitando errores y advertencias de PHP
      error_reporting(0);
  }

  public function index()
  {
      $data["listadoAgencias"] = $this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("agencias/index", $data);
      $this->load->view("footer");
  }

  public function borrar($id_ag)
  {
      $this->Agencia->eliminar($id_ag);
      $this->session->set_flashdata("confirmacion", "Agencia eliminada correctamente");

      redirect("agencias/index");
  }

  public function nuevo()
  {
      $this->load->view("header");
      $this->load->view("agencias/nuevo");
      $this->load->view("footer");
  }

  public function guardarAgencia()
  {
      // INICIO PROCESO DE SUBIDA DE ARCHIVO //
      $config['upload_path'] = APPPATH . '../uploads/agencias/'; // ruta de subida de archivos AQUI
      $config['allowed_types'] = 'jpeg|jpg|png'; // tipo de archivos permitidos
      $config['max_size'] = 5 * 1024; // definir el peso maximo de subida (5MB)
      $nombre_aleatorio = "agencia_" . time() * rand(100, 10000); // creando un nombre aleatorio AQUI
      $config['file_name'] = $nombre_aleatorio; // asignando el nombre al archivo subido
      $this->load->library('upload', $config); // cargando la libreria UPLOAD
      if ($this->upload->do_upload("fotografia_ba")) { // intentando subir el archivo AQUI
          $dataArchivoSubido = $this->upload->data(); // capturando informacion del archivo subido
          $nombre_archivo_subido = $dataArchivoSubido["file_name"]; // obteniendo el nombre del archivo
      } else {
          $nombre_archivo_subido = ""; // Cuando no se sube el archivo el nombre queda VACIO
      }

      $datosNuevoAgencia = array(
          "nombre_ba" => $this->input->post("nombre_ba"),
          "direccion_ba" => $this->input->post("direccion_ba"),
          "ciudad_ba" => $this->input->post("ciudad_ba"),
          "provincia_ba" => $this->input->post("provincia_ba"),
          "pais_ba" => $this->input->post("pais_ba"),
          "telefono_ba" => $this->input->post("telefono_ba"),
          "email_ba" => $this->input->post("email_ba"),
          "fotografia_ba" => $nombre_archivo_subido,
          "latitud_ba" => $this->input->post("latitud_ba"),
          "longitud_ba" => $this->input->post("longitud_ba")
      );

      // Insertar el nuevo registro en la base de datos
      $this->Agencia->insertar($datosNuevoAgencia);

      // Configurar mensaje de confirmación
      $this->session->set_flashdata("confirmacion", "Agencia guardada exitosamente");

      // Redireccionar a la página de listado de agencias
      redirect('agencias/index');
  }

  public function editar($id)
  {
      $data["agenciaEditar"] = $this->Agencia->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("agencias/editar", $data);
      $this->load->view("footer");
  }

  public function actualizarAgencia()
  {
      $id_ag = $this->input->post("id_ag");
      $datosAgencia = array(
          "nombre_ba" => $this->input->post("nombre_ba"),
          "direccion_ba" => $this->input->post("direccion_ba"),
          "ciudad_ba" => $this->input->post("ciudad_ba"),
          "provincia_ba" => $this->input->post("provincia_ba"),
          "pais_ba" => $this->input->post("pais_ba"),
          "telefono_ba" => $this->input->post("telefono_ba"),
          "email_ba" => $this->input->post("email_ba"),
          "fotografia_ba" => $this->input->post("fotografia_ba"),
          "latitud_ba" => $this->input->post("latitud_ba"),
          "longitud_ba" => $this->input->post("longitud_ba")
      );
      $this->Agencia->actualizar($id_ag, $datosAgencia);
      $this->session->set_flashdata("confirmacion", "Agencia actualizada exitosamente");
      redirect('agencias/index');
  }
}

 ?>
