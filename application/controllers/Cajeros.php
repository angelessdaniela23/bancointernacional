<?php

  class Cajeros extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model("Cajero");

      //desabilitando errores y advertencias de php
      error_reporting(0);
    }
//Renderizacion de la vista index de hospitales
  public function index(){
    $data["listadoCajeros"]=$this->Cajero->consultarTodos();
    $this->load->view("header");
    $this->load->view("cajeros/index",$data);
    $this->load->view("footer");
    }

    //ELIMINACION DE HOSPITALES RECIBINEDO EL ID POR get
    public function borrar($id_ca){
      $this->Cajero->eliminar($id_ca);
      $this->session->set_flashdata("confirmacion","Cajero eliminado correctamente");

      redirect("cajeros/index");
    }
    //renderizacion del formulario del nuevo hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("cajeros/nuevo");
      $this->load->view("footer");
    }
    //capturando datos e insterdando  en hospitales
    public function guardarCajero(){


      $datosNuevoCajero=array(
        "nombre_ba"=>$this->input->post("nombre_ba"),
        "ubicacion_ba"=>$this->input->post("ubicacion_ba"),
        "ciudad_ba"=>$this->input->post("ciudad_ba"),
        "provincia_ba"=>$this->input->post("provincia_ba"),
        "pais_ba"=>$this->input->post("pais_ba"),
        "fecha_instalacion_ba"=>$this->input->post("fecha_instalacion_ba"),
        "latitud_ba"=>$this->input->post("latitud_ba"),
        "longitud_ba"=>$this->input->post("longitud_ba")
      );

      //set_flashdata sirve para crear una session de tipo flash
      $this->Cajero->insertar($datosNuevoCajero);
      $this->session->set_flashdata("confirmacion", "Cajero guardado exitosamente");
      enviarEmail("angelesdaniela321@gmail.com","CREACION",
          "<h1>SE CREO EL CAJERO </h1>".$datosNuevoCajero['nombre_ba']);
      redirect('cajeros/index');
    }

    //RENDERIZAR FORMULARIO DE EDICION

      public function editar($id){
        $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("cajero/editar",$data);
        $this->load->view("footer");
      }

      public function actualizarCajero(){
      $id_ca=$this->input->post("id_ca");
      $datosCajero=array(
        "nombre_ba"=>$this->input->post("nombre_ba"),
        "ubicacion_ba"=>$this->input->post("ubicacion_ba"),
        "ciudad_ba"=>$this->input->post("ciudad_ba"),
        "provincia_ba"=>$this->input->post("provincia_ba"),
        "pais_ba"=>$this->input->post("pais_ba"),
        "fecha_instalacion_ba"=>$this->input->post("fecha_instalacion_ba"),
        "latitud_ba"=>$this->input->post("latitud_ba"),
        "longitud_ba"=>$this->input->post("longitud_ba")
      );
      $this->Cajero->actualizar($id_ca,$datosCajero);
      $this->session->set_flashdata("confirmacion",
      "Cajero actualizado exitosamente");
      redirect('cajeros/index');
    }





  }//cierre de la clase

 ?>
