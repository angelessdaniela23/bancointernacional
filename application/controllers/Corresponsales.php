<?php

  class Corresponsales extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model("Corresponsal");

      //desabilitando errores y advertencias de php
      error_reporting(0);
    }
//Renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoCorresponsales"]=$this->Corresponsal->consultarTodos();
      $this->load->view("header");
      $this->load->view("corresponsales/index",$data);
      $this->load->view("footer");
      }

    //ELIMINACION DE HOSPITALES RECIBINEDO EL ID POR get
    public function borrar($id_coc){
      $this->Corresponsal->eliminar($id_co);
      $this->session->set_flashdata("confirmacion","Corresponsal eliminada correctamente");

      redirect("corresponsales/index");
    }
    //renderizacion del formulario del nuevo hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("corresponsales/nuevo");
      $this->load->view("footer");
    }
    //capturando datos e insterdando  en hospitales
    public function guardarCorresponsal(){


      $datosNuevoCorresponsal=array(
        "nombre_ba"=>$this->input->post("nombre_ba"),
        "direccion_ba"=>$this->input->post("direccion_ba"),
        "ciudad_ba"=>$this->input->post("ciudad_ba"),
        "provincia_ba"=>$this->input->post("provincia_ba"),
        "pais_ba"=>$this->input->post("pais_ba"),
        "tipo_servicio_ba"=>$this->input->post("tipo_servicio_ba"),
        "latitud_ba"=>$this->input->post("latitud_ba"),
        "longitud_ba"=>$this->input->post("longitud_ba")

      //set_flashdata sirve para crear una session de tipo flash
      $this->Corresponsal->insertar($datosNuevoCorresponsal);
      $this->session->set_flashdata("confirmacion", "Corresponsal guardada exitosamente");
      enviarEmail("angelesdaniela321@gmail.com","CREACION",
          "<h1>SE CREO LA CORRESPONSAL </h1>".$datosNuevoCorresponsal['nombre_ba']);
      redirect('corresponsales/index');
    }

    //RENDERIZAR FORMULARIO DE EDICION

      public function editar($id){
        $data["corresponsalEditar"]=$this->Corresponsal->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("corresponsales/editar",$data);
        $this->load->view("footer");
      }

      public function actualizarCorresponsal(){
      $id_co=$this->input->post("id_co");
      $datosCorresponsal=array(
        "nombre_ba"=>$this->input->post("nombre_ba"),
        "direccion_ba"=>$this->input->post("direccion_ba"),
        "ciudad_ba"=>$this->input->post("ciudad_ba"),
        "provincia_ba"=>$this->input->post("provincia_ba"),
        "pais_ba"=>$this->input->post("pais_ba"),
        "tipo_servicio_ba"=>$this->input->post("tipo_servicio_ba"),
        "latitud_ba"=>$this->input->post("latitud_ba"),
        "longitud_ba"=>$this->input->post("longitud_ba")
      );
      $this->Corresponsal->actualizar($id_co,$datosCorresponsal);
      $this->session->set_flashdata("confirmacion",
      "Corresponsal actualizada exitosamente");
      redirect('corresponsales/index');
    }





  }//cierre de la clase

 ?>
